# hometask-8

## Ответьте на вопросы

#### 1. Дайте определение переменным. Как можно их объявить?
> Ответ: Именованная структура в памяти программы для хранения данных. С помощью ключевых слов var, let, const
#### 2. Перечислите все примитивные типы данных. В чём их отличие от ссылочных типов данных?
> Ответ: string number boolean undefined null. Ссылочные типы хранят ссылку на объект, поэтому сравниваются и передаются в функции по ссылке, а не копируются в функцию и сравниваются по значению.
#### 3. Что такое массив? Как можно обратиться к элементу массива?
> Ответ: Упорядоченная структура данных, каждый элемент который идентифицируются по индексу. К элементу можно обратиться через индекс, например - "arraySample[2]".
#### 4. Перечислите все из известных вам методов массива, при вызове которых возвращается новый массив.
> Ответ: concat slice split join
#### 5. Объявите переменную типа `object`. В объекте должны лежать поля `name` и `age`.
> Ответ: var sample = {
  name: 'Ильяс',
  age: 25
}; 

## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `tasks.js`;
* Проверяйте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
